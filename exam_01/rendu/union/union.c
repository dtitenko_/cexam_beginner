/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   union.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 10:49:53 by exam              #+#    #+#             */
/*   Updated: 2016/12/27 11:10:22 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void 	ft_putstr(char *str)
{
	while(*str)
	{
		write(1, &(*str), 1);
		str++;
	}
}

int		ft_instr(char *str, char c)
{
	while(*str && *str != c)
		str++;
	return (*str == c);
}

int 	main(int argc, char **argv)
{
	char res[128];
	int i;
	int j;
	int k;

	if (argc == 3)
	{
		k = -1;
		while(++k < 128)
			res[k] = '\0';
		j = -1;
		k = 0;
		while (++k < argc)
		{
			i = -1;
			while(argv[k][++i])
			{
				if (!ft_instr(res, argv[k][i]))
					res[++j] = argv[k][i];
			}
		}
		ft_putstr(res);	
	}
	write (1, "\n", 1);
}
