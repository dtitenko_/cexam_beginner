/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 11:52:43 by exam              #+#    #+#             */
/*   Updated: 2016/12/29 14:59:56 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		get_size_for_base(int n, int base)
{
	int i;

	i = 0;
	if (n < 0 && base == 10)
		i++;
	while (n)
	{
		i++;
		n /= base;
	}
	return (i);
}

char	get_base_char(int i)
{
	if (i < 10)
		return ('0' + i);
	else
		return ('A' + (i - 10));
}

char	*ft_itoa_base(int value, int base)
{
	char			*res;
	int				len;
	unsigned int	tmp;
	
	len	= get_size_for_base(value, base);
	tmp = value;
	if (!((res = (char *)malloc(len + 1)) && base >= 2 && base <= 16))
		return (char *)0;
	if (value < 0)
	{	
		if(base == 10)
		{
			res[0] = '-';
		}
		tmp = -value;
	}
	res[len] = '\0';
	while (tmp)
	{
		--len;
		res[len] = get_base_char(tmp % base);
		tmp /= base;
	}
	return (res);
}
