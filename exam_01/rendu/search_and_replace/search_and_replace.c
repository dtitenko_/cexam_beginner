/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_and_replace.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 10:27:45 by exam              #+#    #+#             */
/*   Updated: 2016/12/27 10:44:23 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void ft_putstr(char *str)
{
	while(*str)
	{
		write(1, &(*str), 1);
		str++;
	}
}

int main(int argc, char **argv)
{
	char	which;
	char	rep;
	char	*str;
	char	*save;

	if (argc == 4 && argv[2][1] == '\0' && argv[3][1] == '\0')
	{
		str = argv[1];
		save = str;
		which = *(argv[2]);
		rep = *(argv[3]);
		while (*str)
		{
			*str = (*str == which) ? rep : *str;
			str++;
		}
		ft_putstr(save);
	}
	write(1, "\n", 1);
}

