/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pgcd.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/27 11:19:32 by exam              #+#    #+#             */
/*   Updated: 2016/12/27 11:28:55 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	unsigned int a;
	unsigned int b;
	unsigned int denom;
	unsigned int i;

	if (argc == 3)
	{
		a = atoi(argv[1]);
		b = atoi(argv[2]);
		denom  = 1;
		i = 1;
		while (++i < a && i < b)
			if (!(a % i) && !(b % i))
				denom = i;
		printf("%i", denom);
	}
	printf("\n");
}
