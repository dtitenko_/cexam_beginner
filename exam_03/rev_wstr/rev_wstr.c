/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_wstr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 11:18:23 by exam              #+#    #+#             */
/*   Updated: 2017/05/09 12:00:08 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

#define ISSPACE(a)	(a == '\t' || a == '\v' || a == ' ')

void	ft_print_s2e(char *start, char *end)
{
	write(1, start, end - start);
}

char	*ft_find_prev_space(char *start, char *beg_str)
{
	start--;
	while (start > beg_str && !ISSPACE(*start))
		start--;
	start = (start < beg_str) ? beg_str : start;
	return (start);
}

size_t	ft_strlen(char *str)
{
	size_t	l;

	l = 0;
	while (*str++)
		l++;
	return (l);
}

void	ft_printwords(char *str)
{
	char	*end;
	char	*start;

	end = str + ft_strlen(str);
	start = end;
	while (end != str)
	{
		start = ft_find_prev_space(end, str);
		if (ISSPACE(*start))
			ft_print_s2e(start + 1, end);
		else
			ft_print_s2e(start, end);
		if (start != str)
			write(1, " ", 1);
		end = start;
	}
}

int		main(int argc, char **argv)
{
	if (argc == 2)
		ft_printwords(argv[1]);
	write(1, "\n", 1);
}
