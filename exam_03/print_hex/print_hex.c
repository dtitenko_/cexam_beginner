/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_hex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 10:47:55 by exam              #+#    #+#             */
/*   Updated: 2017/05/09 11:11:25 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

#define ISNUM(a) ('0' <= a && a <= '9')
#define GET_HEX(a) ((a < 10) ? '0' + a : 'a' + a - 10)

void		ft_putchar(char a)
{
	write(1, &a, 1);
}

long long	ft_atol(char *str)
{
	long long	n;

	n = 0;
	while (ISNUM(*str))
	{
		n = n * 10 + *str - '0';
		str++;
	}
	return (n);
}

void		ft_printhex(long long num)
{
	if (num > 15)
	{
		ft_printhex(num / 16);
		ft_printhex(num % 16);
	}
	else
		ft_putchar(GET_HEX(num));
}

int			main(int argc, char **argv)
{
	long long	n;

	if (argc == 2)
	{
		n = ft_atol(argv[1]);
		ft_printhex(n);
	}
	ft_putchar('\n');
}
