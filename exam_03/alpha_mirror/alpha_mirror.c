/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alpha_mirror.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 10:14:26 by exam              #+#    #+#             */
/*   Updated: 2017/05/09 10:41:59 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#define ISLOWER(a)	('a' <= a && a <= 'z')
#define ISUPPER(a)	('A' <= a && a <= 'Z')
#define ISALPHA(a)	(('a' <= a && a <= 'z') || ('A' <= a && a <= 'Z'))
#define LA2LZ(a)	('z' - a + 'a')
#define A2Z(a)		('Z' - a + 'A')
#define LZ2LA(a)	('a' +'z' - a)
#define Z2A(a)		('A' + 'Z' - a)

void	ft_putstr(char *str)
{
	while (*str)
	{
		write(1, str, 1);
		str++;
	}
}

int		main(int argc, char **argv)
{
	int	i;

	i = -1;
	if (argc == 2)
	{
		while (argv[1][++i])
			if (ISALPHA(argv[1][i]))
			{
				if (ISLOWER(argv[1][i]))
					if (argv[1][i] <= 'm')
						argv[1][i] = LA2LZ(argv[1][i]);
					else
						argv[1][i] = LZ2LA(argv[1][i]);
				else if (argv[1][i] <= 'M')
					argv[1][i] = A2Z(argv[1][i]);
				else
					argv[1][i] = Z2A(argv[1][i]);
			}
		ft_putstr(argv[1]);
	}
	ft_putstr("\n");
}
