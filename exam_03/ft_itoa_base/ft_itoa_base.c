/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 12:02:43 by exam              #+#    #+#             */
/*   Updated: 2017/05/09 12:39:20 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#define GET_CHR(a)	((a < 10) ? '0' + a : 'A' + a - 10)

int		get_len(int v, int b)
{
	int				l;
	unsigned int	uv;

	l = 1;
	l += (v < 0 && b == 10) ? 1 : 0;
	uv = (v < 0) ? -v : v;
	uv /= b;
	while (uv)
	{
		l++;
		uv /= b;
	}
	return (l);
}

char	*ft_itoa_base(int value, int base)
{
	char			*num;
	int				len;
	unsigned int	n;

	len = get_len(value, base);
	if (!(num = (char *)malloc(sizeof(char) * len + 1)))
		return (NULL);
	n = (value < 0) ? -value : value;
	(value < 0 && base == 10) ? num[0] = '-' : 0;
	num[len] = 0;
	while (len--)
	{
		num[len] = GET_CHR(n % base);
		n /= base;
		if (n == 0)
			break ;
	}
	return (num);
}
